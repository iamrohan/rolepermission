@extends('layouts.app')

@section('content')
<h1>Edit User</h1>
<div class="col-md-4 offset-md-4">

    <form method="post" action="{{route('updateuser', $user)}}">
        @csrf
        @method('put')
        <label>Name</label>
        <input type="text" class="form-control" name="name" value={{$user->name}} >
        <label>Email</label>
        <input type="text" class="form-control" name="email" value={{$user->email}} >
        <label>Roles</label>
            <select class="form-control" name="role">
                @if ($role)
                    <option value="">{{$role}}</option>
                @endif
                @foreach($roles as $r)
                    @if($r->name !=$role)
                        <option value="{{$r->name}}">{{$r->name}}</option>
                    @endif
                @endforeach
                
            </select>
        <br/>
        <button type="submit" class="btn btn-success form-control">Update</button>
    </form>
</div>


@endsection