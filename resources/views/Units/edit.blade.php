@extends('layouts.app')

@section('content')
<h1>Edit Unit</h1>
<div class="col-md-4 offset-md-4">

    <form method="post" action="{{route('updateUnit', $unit)}}">
        @csrf
        @method('put')
        <label>Name</label>
        <input type="text" class="form-control" name="name" value={{$unit->name}} >

        @error('name')
            <li class="list" style="color:red;">{{$message}}</li>
        @enderror
        <br/>
        <button type="submit" class="btn btn-success form-control">Update</button>
    </form>
</div>


@endsection