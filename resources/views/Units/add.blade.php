@extends('layouts.app')

@section('content')
<h1>Add Unit</h1>
<div class="col-md-4 offset-md-4">

    {{-- @if(session()->has('unitsuccess'))
        <div class="alert alert-success">
            {{session()->get('unitsuccess')}}
        </div>
        {{session()->forget('unitsuccess')}}
    @endif --}}
    <form method="post" action="{{route('add')}}">
        @csrf
        <label>Name</label>
        <input type="text" class="form-control" name="name" >
        @error('name')
            <li class="list" style="color:red;">{{$message}}</li>
        @enderror
        <br/>
        <button type="submit" class="btn btn-success form-control">Add</button>
    </form>
</div>
@endsection