@extends('layouts.app')

@section('content')
<h1>Edit Role</h1>
<div class="col-md-4 offset-md-4">
    @php
        $pernames = $permissions->pluck('name');
        $models=array('Role','User','Unit');
        
        $modelper=array('role','user','unit');
        $operation=array('create','edit','view','delete');
        $countmodelper=count($modelper);
        $countoperation=count($operation);
    @endphp
    {{-- @dd($pernames) --}}
    <form method="post" action="{{route('updaterole', $role)}}">
        @csrf
        @method('put')
        <label>Name</label>
        <input type="text" class="form-control" name="name" value={{$role->name}} >
        <br/>

        <table class="table">
            <thead>
              <tr>
                <th scope="col">Model</th>
                <th scope="col">Create</th>
                <th scope="col">Edit</th>
                <th scope="col">View</th>
                <th scope="col">Delete</th>
              </tr>
            </thead>
            <tbody>

                @foreach ($models as $item)
                  <tr>
                    <td>{{$item}}</td>
                    @foreach ($operation as $op)
                      <td>
                        <input name="perm[]" value={{strtolower($item).'_'.$op}} type="checkbox" aria-label="Checkbox for following text input" {{$pernames->contains(strtolower($item).'_'.$op) ? "checked" :""}}  />
                      </td>
                    @endforeach
                  </tr>
                @endforeach
              {{-- <tr>
                <th scope="row">Role</th>
                <td><input name="check[]" value="role_create" type="checkbox" {{$pernames->contains('role_create') ? 'checked' : ''}} aria-label="Checkbox for following text input"></td>
                <td><input name="check[]" value="role_edit" type="checkbox" {{$pernames->contains('role_edit') ? 'checked' : ''}} aria-label="Checkbox for following text input"></td>
                <td><input name="check[]" value="role_view" type="checkbox" {{$pernames->contains('role_view') ? 'checked' : ''}} aria-label="Checkbox for following text input"></td>
                <td><input name="check[]" value="role_delete" type="checkbox" {{$pernames->contains('role_delete') ? 'checked' : ''}} aria-label="Checkbox for following text input"></td>
              </tr>
              <tr>
                <th scope="row">Unit</th>
                <td><input name="check[]" value="unit_create" type="checkbox" {{$pernames->contains('unit_create') ? 'checked' : ''}} aria-label="Checkbox for following text input"></td>
                <td><input name="check[]" value="unit_edit" type="checkbox" {{$pernames->contains('unit_edit') ? 'checked' : ''}} aria-label="Checkbox for following text input"></td>
                <td><input name="check[]" value="unit_view" type="checkbox" {{$pernames->contains('unit_view') ? 'checked' : ''}} aria-label="Checkbox for following text input"></td>
                <td><input name="check[]" value="unit_delete" type="checkbox" {{$pernames->contains('unit_delete') ? 'checked' : ''}} aria-label="Checkbox for following text input"></td>
              </tr>
              <tr>
                <th scope="row">User</th>
                <td><input name="check[]" value="user_create" type="checkbox" {{$pernames->contains('user_create') ? 'checked' : ''}} aria-label="Checkbox for following text input"></td>
                <td><input name="check[]" value="user_edit" type="checkbox" {{$pernames->contains('user_edit') ? 'checked' : ''}} aria-label="Checkbox for following text input"></td>
                <td><input name="check[]" value="user_view" type="checkbox" {{$pernames->contains('user_view') ? 'checked' : ''}} aria-label="Checkbox for following text input"></td>
                <td><input name="check[]" value="user_delete" type="checkbox" {{$pernames->contains('user_delete') ? 'checked' : ''}} aria-label="Checkbox for following text input"></td>
              </tr> --}}
            </tbody>
        </table>
        <button type="submit" class="btn btn-success form-control">Update</button>
    </form>
</div>


@endsection