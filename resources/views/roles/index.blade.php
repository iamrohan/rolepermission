@extends('layouts.app')

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
    <h1 class="h2">Roles</h1>
</div>
<div class="container-fluid">
    <div class="table">

        <a href="{{route('createrole')}}" class="btn btn-primary" style="margin-bottom: 8px;margin-left:550px;">Add Role</a>
        {{-- @if(session()->has('updateunit'))
            <div class="alert alert-success">
                {{session()->get('updateunit')}}
            </div>
            {{session()->forget('updateunit')}}
        @endif

        @if(session()->has('deleteunit'))
            <div class="alert alert-danger">
                {{session()->get('deleteunit')}}
            </div>
            {{session()->forget('deleteunit')}}
        @endif --}}
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">Name</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($roles as $role)
                    <tr>
                    <td>{{$role->id}}</td>
                    <td>{{$role->name}}</td>
                    <td>

                        <a href="{{route('editrole', $role)}}" class="btn btn-success" style="margin-bottom: 5px;">Edit</a>

                        <form action="{{route('deleterole', $role)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">
                                Delete
                            </button>
                        </form>
                    </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>
  
@endsection