@extends('layouts.app')

@section('content')
<h1>Add Role</h1>
<div class="col-md-4 offset-md-3">

    {{-- @if(session()->has('unitsuccess'))
        <div class="alert alert-success">
            {{session()->get('unitsuccess')}}
        </div>
        {{session()->forget('unitsuccess')}}
    @endif --}}
    <form method="post" action="{{route('addrole')}}">
        @csrf
        <div style="margin-left:10px;">
            <label>Name</label>
            <input type="text" class="form-control" name="names" >
        </div>
        
        <br/>
        <table class="table">
            <thead>
              <tr>
                <th scope="col">Model</th>
                <th scope="col">Create</th>
                <th scope="col">Edit</th>
                <th scope="col">View</th>
                <th scope="col">Delete</th>
              </tr>
            </thead>
            <tbody>

              @php
                $models=['Role','User','Unit'];
                $modelper=['role','user','unit'];
                $operation=['create','edit','view','delete'];
              @endphp

              @foreach($models as $mp)
                <tr>
                  <td>{{$mp}}</td>
                  @foreach($operation as $op)
                  <td><input name="perm[]" value={{ strtolower($mp)."_".$op}} type="checkbox" aria-label="Checkbox for following text input"></td>
                  @endforeach
                </tr>
              @endforeach
              {{-- <tr>
                <th scope="row">Role</th>
                <td><input name="check[]" value="role_create" type="checkbox" aria-label="Checkbox for following text input"></td>
                <td><input name="check[]" value="role_edit" type="checkbox" aria-label="Checkbox for following text input"></td>
                <td><input name="check[]" value="role_view" type="checkbox" aria-label="Checkbox for following text input"></td>
                <td><input name="check[]" value="role_delete" type="checkbox" aria-label="Checkbox for following text input"></td>
              </tr>
              <tr>
                <th scope="row">Unit</th>
                <td><input name="check[]" value="unit_create" type="checkbox" aria-label="Checkbox for following text input"></td>
                <td><input name="check[]" value="unit_edit" type="checkbox" aria-label="Checkbox for following text input"></td>
                <td><input name="check[]" value="unit_view" type="checkbox" aria-label="Checkbox for following text input"></td>
                <td><input name="check[]" value="unit_delete" type="checkbox" aria-label="Checkbox for following text input"></td>
              </tr>
              <tr>
                <th scope="row">User</th>
                <td><input name="check[]" value="user_create" type="checkbox" aria-label="Checkbox for following text input"></td>
                <td><input name="check[]" value="user_edit" type="checkbox" aria-label="Checkbox for following text input"></td>
                <td><input name="check[]" value="user_view" type="checkbox" aria-label="Checkbox for following text input"></td>
                <td><input name="check[]" value="user_delete" type="checkbox" aria-label="Checkbox for following text input"></td>
              </tr> --}}
            </tbody>
        </table>
        <button type="submit" class="btn btn-success form-control" style="margin-left:10px;">Add Role</button>
    </form>
</div>
@endsection