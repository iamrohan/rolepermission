<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;

class UserController extends Controller
{
    public function index()
    {
        $roles = Role::all();
        $users = User::all();
        // dd($users);
        // $role = Role::where('name', )
        return view('users.index', [
            'users' => $users,
            'roles' => $roles
        ]);
    }


    public function create()
    {   $roles=Role::all();
        return view('users.add', [ 
            'roles' => $roles
        ]);
    }


    public function store(Request $request)
    {
        // dd($request->all());
        // $this->validate($request, [
        //     'name' => 'required|max:255',
        //     'email' => 'required|email|max:255',
        //     'password' => 'required',
        // ]);
        // //dd($request->all());
        // User::create([
        //     'name' => $request->name,
        //     'email' => $request->email,
        //     'role'   => $request->role,
        //     'password' => Hash::make($request->password),
        //  ]);
        //$input = $request->all();
        // $input['password'] = Hash::make($input['password']);
        // $user = User::create($input);
        // // dd($user);
        // $user -> assignRole($request->input('role'));
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $role_data = Role::find($request->role);
        //dd($role_data->name);
        $user->role=$role_data->name;
        $user->assignRole($role_data);
        $perm_ids = DB::table('role_has_permissions')->where('role_id', $role_data->id)->get();
        //dd($perm_ids);
        //dd($input);
        // $user = new User();
        // $user->name = $request->name;
        // $user->email = $request->email;
        // $user->password = Hash::make($request->password);
        // $role_data = Role::find($request->role);
        // print($role_data);
        // $user->role=$role_data->name;
        // $user->assignRole($request->role);
        // $role = Role::findById($request->role);
        // $perm_ids = DB::table('role_has_permissions')->where('role_id', $role->id)->get();

        foreach ($perm_ids as $perm) {
            $permss=Permission::findById($perm->permission_id);
            // dd($permss);
            $user->givePermissionTo($permss->name);
        }
        $user->save();
        return redirect('users');
        
    }


    public function show(Unit $unit)
    {
        //
    }

    

    public function edit(User $user)
    {
        // dd('hello');
        $user = User::find($user->id);
        $role = $user->role; //helps to find user's role
        $roles = Role::all();
        // print($role);
        return view('users.edit',[
            'role' => $role,
            'roles' => $roles,            
            'user' => $user
        ]);
    }

 
    public function update(Request $request, User $user)
    {
        // dd('hello');
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
        ]);
        // $user->update([
        //     'name' =>$request->name,
        //     'email' =>$request->email,
        // ]);
        $input = $request->all();
        // dd($input);
        $user = User::find($user->id);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id', $user->id)->delete();
        $user->assignRole($request->input('role'));
        // dd($user);
        return redirect('users');

    }


    public function destroy(User $user)
    {
        $user->delete();
        return redirect('users');
    }

    public function logout() {
        Auth::logout();
        return redirect('/home');
    }
}
