<?php

namespace App\Http\Controllers;

use App\Unit;
use Illuminate\Http\Request;


class UnitController extends Controller
{
    public function index(Request $request)
    {
        $units = Unit::all();
        if($request->wantsJson()){
           return response()->json($units);
        }
        return view('units.index', [
            'units' => $units
        ]);
    }


    public function create()
    {
        return view('units.add');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        if($request->wantsJson()){
            // $this->validate($request, [
            //     'name' => required,
            // ]);
            $units = Unit::create($request->all());
            return response()->json($units);
        }
        
        Unit::create($request->only('name'));
        return redirect('units');
        
    }


    public function show(Unit $unit)
    {
        //
    }

    

    public function edit(Unit $unit)
    {
        // dd('hello');
        return view('units.edit',[
            'unit' => $unit
        ]);
    }

 
    public function update(Request $request, Unit $unit)
    {
        // dd('hello');
        // $this->validate($request, [
        //     'name' => 'required'
        // ]);
        if ($request->wantsJson()) {
            $unit->update($request->all());
            return response()->json($unit);
        }
        $unit->update($request->only('name'));
        return redirect('units');

    }


    public function destroy(Request $request, Unit $unit)
    {
        if($request->wantsJson()){
            $unit->delete();
            return response() -> json(null);
        }
        $unit->delete();
        return redirect('units');
    }
}
