<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
     
    public function index(Request $request) {
        if($request->wantsJson()){
            $roles = Role::all();
            return response()->json($roles);
        }
        $roles = Role::all();
        return view('roles.index', [
            'roles' => $roles
        ]);
    }

    public function create()
    {
        return view('roles.add');
    }

    public function store(Request $request)
    {
        // dd('hello');
        $this->validate($request, [
            'names' => 'required'
        ]);

        if($request->wantsJson()){
            $role = new Role();
            $role->name = $request->names;
            $data[] = $request->perm;
            $role->givePermissionTo($data);
            $role->save();
            return response()->json($role);

        }

        $role = new Role();
        $role->name = $request->name;
        $data[] = $request->perm;
        $role->givePermissionTo($data);
        $role->save();
            // dd($data);
            // $role = Role::find($id);
            
        // Role::create($request->only('name'));
        return redirect('roles');
        
    }

    public function show(Role $role)
    {
        //
    }

    public function edit(Request $request, $id)
    {
        // if($request->wantsJson()){
        //     $role = Role::find($id);
        //     $permissions = $role->permissions;
        //     return response()->json(['role'=>$val["role"],'permissions'=>$val["permissions"]]);
        // }
        // dd('hello');
        $role = Role::find($id);
        $permissions = $role->permissions;
        //dd($permissions);
        return view('roles.edit',[
            'role' => $role,
            'permissions' => $permissions
        ]);
    }

    public function update(Request $request, Role $role)
    {
        // dd($request->all());
        if($request->wantsJson()){
            $role = Role::find($role->id);
            $role->name = $request->name;
            $perms[] = $request->perm;
            $role->syncPermissions($perms);
            $role->save();
            return response()->json($role);
        }
        $role = Role::find($role->id);
        $role->name = $request->name;
        $perms[] = $request->perm;
        //dd($perms);
        $role->syncPermissions($perms);
        $role->save();

        return redirect('roles');

    }

    public function destroy(Request $request, Role $role)
    {
        // dd($role);
        if($request->wantsJson()) {
            $role -> delete();
            return response()->json(null);
        }
        $role->delete();
        return redirect('roles');
    }
}
