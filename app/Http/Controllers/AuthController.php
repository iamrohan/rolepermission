<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(Request $request, User $user){
        $this->validate($request, [
            'name' => 'required',
            'email' =>'email|required',
            'password' => 'required|min:6'
        ]);
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'role' => $request->role,
            'password' => Hash::make($request->password)
        ]);

        $token=$user->createToken("rohan")->accessToken;
        return response()->json(['token'=>$token]);
    }

    public function login(Request $request) {
        $this->validate($request, [
            'email' =>'email|required',
            'password' => 'required|min:6'
        ]);

        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (Auth::attempt($credentials)) {
            $token=auth()->user()->createToken("rohan")->accessToken;
            return response()->json(['token'=>$token]);
        } else {
            return response() -> json(['Login' => "No Login Access"]);
        }
    }
}
