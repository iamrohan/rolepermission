<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions =['role_create', 'role_view', 'role_edit', 'role_delete','user_create', 'user_view', 'user_edit', 'user_delete', 'unit_create', 'unit_view', 'unit_edit', 'unit_delete'];

        foreach($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }
    }
}
