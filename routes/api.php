<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UnitController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//routes for login and register
Route::post('/register',[AuthController::class, 'register']);
Route::post('/login',[AuthController::class, 'login']);

//routes for Units
Route::get('/units', [UnitController::class, 'index'])->name('units');
Route::get('/units/create', [UnitController::class, 'create'])->name('create');
Route::post('/units', [UnitController::class, 'store'])->name('add');
Route::get('/units/{unit}/edit',[UnitController::class, 'edit'])->name('edit');
Route::put('/units/{unit}',[UnitController::class, 'update'])->name('updateUnit');
Route::delete('/units/{unit}', [UnitController::class, 'destroy'])->name('delete');

//routes for roles
Route::get('/roles', [RoleController::class, 'index'])->name('roles');
Route::get('/roles/create', [RoleController::class, 'create'])->name('createrole');
Route::post('/roles', [RoleController::class, 'store'])->name('addrole');
Route::get('/roles/{role}/edit',[RoleController::class, 'edit'])->name('editrole');
Route::put('/roles/{role}',[RoleController::class, 'update'])->name('updaterole');
Route::delete('/roles/{role}', [RoleController::class, 'destroy'])->name('deleterole');
