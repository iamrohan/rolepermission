<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UnitController;
use App\Http\Controllers\UserController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//routes for Units
Route::get('/units', [UnitController::class, 'index'])->name('units');
Route::get('/units/create', [UnitController::class, 'create'])->name('create');
Route::post('/units', [UnitController::class, 'store'])->name('add');
Route::get('/units/{unit}/edit',[UnitController::class, 'edit'])->name('edit');
Route::put('/units/{unit}',[UnitController::class, 'update'])->name('updateUnit');
Route::delete('/units/{unit}', [UnitController::class, 'destroy'])->name('delete');

//routes for users
Route::get('/users', [UserController::class, 'index'])->name('users');
Route::get('/users/create', [UserController::class, 'create'])->name('createuser');
Route::post('/users', [UserController::class, 'store'])->name('adduser');
Route::get('/users/{user}/edit',[UserController::class, 'edit'])->name('edituser');
Route::put('/users/{user}',[UserController::class, 'update'])->name('updateuser');
Route::delete('/users/{user}', [UserController::class, 'destroy'])->name('deleteuser');
Route::get('/logout', [UserController::class, 'logout']);

//routes for roles
Route::get('/roles', [RoleController::class, 'index'])->name('roles');
Route::get('/roles/create', [RoleController::class, 'create'])->name('createrole');
Route::post('/roles', [RoleController::class, 'store'])->name('addrole');
Route::get('/roles/{role}/edit',[RoleController::class, 'edit'])->name('editrole');
Route::put('/roles/{role}',[RoleController::class, 'update'])->name('updaterole');
Route::delete('/roles/{role}', [RoleController::class, 'destroy'])->name('deleterole');